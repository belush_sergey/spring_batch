package com.example.springbatchtask;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class NumberTasklet implements Tasklet {

    private String filePath;

    NumberTasklet(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {

        try {
            int sumStream = Files.lines(Paths.get(filePath))
                            .mapToInt(Integer::valueOf)
                            .sum();

            System.out.println("sum = " + sumStream);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return RepeatStatus.FINISHED;
    }
}