package com.example.springbatchtask;

import org.springframework.batch.item.ItemProcessor;

public class NumbersItemProcessor implements ItemProcessor<Numbers, Integer> {

    @Override
    public Integer process(Numbers numbers) {
        return numbers.getNumber3() + 7;
    }

}