package com.example.springbatchtask;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Numbers {

    private int number1;
    private int number2;
    private int number3;
    private int number4;
    private int number5;

    @Override
    public String toString() {
        return "Numbers{" +
                "number1='" + number1 + '\'' +
                ", number2='" + number2 + '\'' +
                ", number3='" + number3 + '\'' +
                ", number4='" + number4 + '\'' +
                ", number5='" + number5 + '\'' +
                '}';
    }
}
