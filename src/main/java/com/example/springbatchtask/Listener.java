package com.example.springbatchtask;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.stereotype.Component;

import static com.example.springbatchtask.ContextKeys.INTERMEDIATE_RESULT_FILE_KEY;

@Component
public class Listener extends ExecutionContext implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {

        jobExecution.getExecutionContext()
                .put(INTERMEDIATE_RESULT_FILE_KEY,
                        "src/main/resources/newFileNumbers.csv");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {

    }

}