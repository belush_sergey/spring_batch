package com.example.springbatchtask;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.transform.PassThroughLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private JobBuilderFactory jobBuilderFactory;
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    public BatchConfiguration(JobBuilderFactory jobBuilderFactory,
                              StepBuilderFactory stepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    @StepScope
    public FlatFileItemReader<Numbers> reader() {
        return new FlatFileItemReaderBuilder<Numbers>()
                .name("numbersItemReader")
                .resource(new ClassPathResource("inputNumbers.csv"))
                .delimited()
                .names(new String[]{"number1", "number2", "number3", "number4", "number5"})
                .targetType(Numbers.class)
                .build();
    }

    @Bean
    @StepScope
    public NumbersItemProcessor processor() {
        return new NumbersItemProcessor();
    }

    @Bean
    @StepScope
    public FlatFileItemWriter<Integer> writer(@Value("#{jobExecutionContext['inter_file']}") String filePath) {
        return new FlatFileItemWriterBuilder<Integer>()
                .name("itemWriter")
                .resource(new FileSystemResource(filePath))
                .lineAggregator(new PassThroughLineAggregator<>())
                .build();
    }

    @Bean
    public Job importNumbersJob(Listener listener,
                                @Qualifier("firstCoolStep") Step firstCoolStep,
                                @Qualifier("secondCoolStep") Step secondCoolStep) {
        return jobBuilderFactory.get("importNumbersJob")
                .start(firstCoolStep)
                .next(secondCoolStep)
                .listener(listener)
                .build();
    }

    @Bean("firstCoolStep")
    @JobScope
    public Step step1(@Qualifier("writer") ItemWriter<Integer> writer,
                      @Qualifier("processor") NumbersItemProcessor processor,
                      @Qualifier("reader") FlatFileItemReader<Numbers> reader) {

        return stepBuilderFactory.get("step1")
                .<Numbers, Integer>chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean("secondCoolStep")
    @JobScope
    public Step step2(@Qualifier("fileReaderTasklet") Tasklet tasklet) {
        return stepBuilderFactory.get("step2").tasklet(tasklet).build();
    }

    @Bean
    @StepScope
    public Tasklet fileReaderTasklet(@Value("#{jobExecutionContext['inter_file']}") String filePath) {
        return new NumberTasklet(filePath);
    }

}
